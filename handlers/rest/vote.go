package rest

import (
	"github.com/gin-gonic/gin"
	"github.com/pkg/errors"
	"gitlab.com/block-chain-voting/chain_mock/handlers/rest/requests"
	"gitlab.com/block-chain-voting/chain_mock/services/smart"
)

type voteCtrl struct {
	client *smart.Service
}

func newVoteCtrl(client *smart.Service) *voteCtrl {
	return &voteCtrl{client: client}
}

func (vc *voteCtrl) FindVoteByVoterPublicKey(ctx *gin.Context) {
	var req requests.FindVoteByVoterPublicKey
	if err := ctx.ShouldBindJSON(&req); err != nil {
		ctx.Error(errors.Wrap(smart.ErrInvalidInput, err.Error()))
		return
	}

	vote, err := vc.client.FindVote(req.CandidateID, req.PublicKey)
	if err != nil {
		ctx.Error(err)
		return
	}

	ctx.JSON(200, vote)
}
