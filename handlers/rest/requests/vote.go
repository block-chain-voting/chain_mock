package requests

type FindVoteByVoterPublicKey struct {
	CandidateID int64  `json:"candidate_id" binding:"required"`
	PublicKey   []byte `json:"public_key" binding:"required"`
}
