package rest

import (
	"net/http"

	"github.com/Shopify/sarama"
	"github.com/gin-gonic/gin"
	"gitlab.com/block-chain-voting/chain_mock/services/smart"
)

func NewHandler(smart *smart.Service) http.Handler {
	vc := newVoteCtrl(smart)

	r := gin.Default()
	r.Use(RequestID(), ErrorHandler())

	{
		api := r.Group("/api")
		api.GET("/vote", vc.FindVoteByVoterPublicKey)
	}

	return r
}

func NewHealthHandler(version string, admin sarama.ClusterAdmin, producer sarama.SyncProducer) http.Handler {
	mux := gin.Default()

	hc := newHealthCtrl(version, admin, producer)
	mux.GET("/ready", hc.ready)

	return mux
}
