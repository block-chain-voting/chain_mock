package rest

import (
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"github.com/pkg/errors"
	"github.com/rs/zerolog/log"
	"gitlab.com/block-chain-voting/chain_mock/apierr"
)

const requestIDCtxKey = "request_id"

type clientError struct {
	Msg string `json:"msg"`
}

func RequestID() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		ctx.Set(requestIDCtxKey, uuid.NewString())
		ctx.Next()
	}
}

func ErrorHandler() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		ctx.Next()

		ginErr := ctx.Errors.Last()
		if ginErr == nil {
			return
		}

		requestID := ctx.GetString(requestIDCtxKey)

		var apiErr *apierr.Error
		if errors.As(ginErr.Err, &apiErr) {
			log.Debug().
				Stack().
				Str(requestIDCtxKey, requestID).
				Err(ginErr.Err).
				Msg("Client error")
			if !ctx.IsAborted() {
				ctx.AbortWithStatusJSON(400, clientError{Msg: ginErr.Error()})
			}
		} else {
			log.Error().
				Str(requestIDCtxKey, requestID).
				Stack().
				Err(ginErr.Err).
				Msg("Internal error")
			if !ctx.IsAborted() {
				ctx.AbortWithStatus(500)
			}
		}
	}
}
