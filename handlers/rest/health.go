package rest

import (
	"github.com/Shopify/sarama"
	"github.com/gin-gonic/gin"
)

const healthTopic = "health"

type healthCtrl struct {
	version  string
	admin    sarama.ClusterAdmin
	producer sarama.SyncProducer
}

func newHealthCtrl(version string, admin sarama.ClusterAdmin, producer sarama.SyncProducer) *healthCtrl {
	return &healthCtrl{
		version:  version,
		admin:    admin,
		producer: producer,
	}
}

func (hc *healthCtrl) ready(ctx *gin.Context) {
	var isErr bool
	res := struct {
		Version string `json:"version"`
		Kafka   string `json:"kafka"`
	}{
		Version: hc.version,
		Kafka:   "ok",
	}

	if err := hc.checkKafka(); err != nil {
		res.Kafka = err.Error()
	}

	if isErr {
		ctx.AbortWithStatusJSON(503, res)
		return
	}
	ctx.JSON(200, res)
}

func (hc *healthCtrl) checkKafka() error {
	if err := hc.admin.CreateTopic(healthTopic, &sarama.TopicDetail{
		NumPartitions:     1,
		ReplicationFactor: 1,
	}, false); err != nil {
		return err
	}

	if _, _, err := hc.producer.SendMessage(&sarama.ProducerMessage{Topic: healthTopic}); err != nil {
		return err
	}

	return hc.admin.DeleteTopic(healthTopic)
}
