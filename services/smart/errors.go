package smart

import (
	"gitlab.com/block-chain-voting/chain_mock/apierr"
)

const (
	ErrInvalidInput = apierr.Error("invalid input")
	ErrInvalidVote  = apierr.Error("vote is invalid")
	ErrVoteNotFound = apierr.Error("vote is not found")
)
