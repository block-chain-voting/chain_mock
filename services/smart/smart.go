package smart

import (
	"bytes"
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"sync"

	"github.com/pkg/errors"
	votingpb "gitlab.com/block-chain-voting/proto"
)

type Service struct {
	data sync.Map // map[int64]*votingpb.VotePersisted
}

func NewService() *Service {
	return &Service{}
}

func (s *Service) RegisterVote(vote *votingpb.Vote) error {
	i, err := x509.ParsePKIXPublicKey(vote.Voter.PublicKey)
	if err != nil {
		return errors.Wrap(err, "failed to parse voter public key")
	}
	pubKey, ok := i.(*rsa.PublicKey)
	if !ok {
		return errors.New("invalid RSA public key")
	}

	passportRSA, err := rsa.EncryptPKCS1v15(rand.Reader, pubKey, []byte(vote.Voter.Passport))
	if err != nil {
		return err
	}

	s.data.Store(vote.CandidateId, &votingpb.VotePersisted{
		VoterPersisted: &votingpb.VoterPersisted{
			PublicKeyRsa: vote.Voter.PublicKey,
			PassportRsa:  passportRSA,
		},
		CampaignId:  vote.CampaignId,
		CandidateId: vote.CandidateId,
		Status:      vote.Status,
		FailReason:  vote.FailReason,
	})

	return nil
}

func (s *Service) FindVote(candidateID int64, voterPublicKey []byte) (*votingpb.VotePersisted, error) {
	val, ok := s.data.Load(candidateID)
	if !ok {
		return nil, errors.Wrapf(ErrVoteNotFound, "candidate ID=%d", candidateID)
	}
	vote := val.(*votingpb.VotePersisted)

	if bytes.Compare(vote.VoterPersisted.PublicKeyRsa, voterPublicKey) != 0 {
		return nil, errors.Wrapf(ErrVoteNotFound, "candidate ID=%d", candidateID)
	}

	return vote, nil
}
